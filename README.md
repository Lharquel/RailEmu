# **RailEmu**

RailEmu est un émulateur Dofus 2.3.7 repris en partant de zéro.
J'utilise le protocole de Sunshine ([Git](https://gitlab.com/Unteraqwx/Sunshine) et merci a UnTer pour son aide)

Mon but dans ce projet est de progresser en C# et en émulation Dofus.
Cet émulateur est avant tout destiné a l'apprentissage. 
Je débute en C# et donc mon code est loin d'être le plus optimal pour ce projet.

# Links
[Changeslog](https://gitlab.com/FantOom/RailEmu/blob/master/ChangesLogs.md)

# Screens
[![alt tag](https://i.imgur.com/TqvXZbi.png)](https://imgur.com/a/w11x4y7)
[![alt tag](https://i.imgur.com/GH77V6I.png)](https://imgur.com/a/w11x4y7)
[![alt tag](https://i.imgur.com/KIdTvgP.png)](https://imgur.com/a/w11x4y7)